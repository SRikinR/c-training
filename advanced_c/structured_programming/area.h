// defining union


typedef union area
{
    float* length;
    float* height;
    float* weidth;
    float* radius;
    float (*fun_ptr)(union area *);
}area_t;

void Square(union area*, int *length);
void Circle(union area*, int *radius);
void Triangle(union area*, int *height, int *weidth);
void Rectangle(union area*,int *length, int *weidth);

void Print(union area *);


