// defining union
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

union area
{
    int* length;
    int* height;
    int* weidth;
    int* radius;
    int* area;
    void (*print)(union area *);
};
typedef union area area_t;

void Square(area_t* S_area, int *length_of_square, int *area_of_square);
void Circle(area_t* C_area, int *radius_of_circle);
void Triangle(area_t* T_area, int *height_of_triangle, int *weidth_of_triangle);
void Rectangle(area_t* R_area,int *length_of_rectangle, int *weidth_of_rectangle);
void Print_area(area_t* int_area);

int main()
{
    int choice;
    printf("Enter the the geometry you need to find area of:\n");
    printf("press\t1->Square\n\t2->Circle\n\t3->Triangle\n\t4->Rectangle\n");
    scanf("%d",&choice);

    switch (choice)
    {
        case 1:
        {
            area_t Square_area;
            int input_length;
            scanf("%d",&input_length);
            int input_area= input_length*input_length;
            printf("Input Area is:%d\n",input_area);
            Square(&Square_area,&input_length,&input_area);
            Square_area.print(&Square_area);
            //int received_area=area_to_find.print(&area_to_find);
            break;
        }

        case 2:
        {
            area_t Circle_area;
            int radius;
            scanf("%d",&radius);
            Circle(&Circle_area,&radius);
            Circle_area.print(&Circle_area);
            break;
        }

        case 3:
        {
            area_t Triangle_area;
            int height,weidth;
            scanf("%d%d",&height,&weidth);
            Triangle(&Triangle_area, &height, &weidth);
            Triangle_area.print(&Triangle_area);
            break;        
        }

        case 4:
        {
            area_t Rect_area;
            int  length ,weidth;
            scanf("%d%d",&length,&weidth);
            Rectangle(&Rect_area, &length, &weidth);
            Rect_area.print(&Rect_area);
            break;        
        }

        default:
            printf("Invalid Input");
            break;
    }

    return EXIT_SUCCESS;
}

void Print_area(area_t *given_geometry)
{
//    return given_geometry->area;
    printf("Area of your geometry is: %d\n",given_geometry->area);
}

void Square(area_t* S_area, int* length_of_square,int *area_of_square)
{
    S_area->length= *length_of_square;
    int temp_len= S_area->length;
    S_area->area= *area_of_square;
    printf("Area of a Square is: %d\n",S_area->area);
    S_area->print=&Print_area;
}

void Circle(area_t* C_area, int *radius_of_circle)
{
    float pi=3.14;
    int temp_radius=*radius_of_circle;
    C_area->radius= *radius_of_circle;
    printf("Entered Radius of Circle is:%d\n",C_area->radius);
    float temp_area =(pi * temp_radius);
    C_area->area=(int )temp_area;
    printf("Area of Circle is: %f\n",temp_area);
    C_area->print=&Print_area;
}

void Triangle(area_t* T_area, int *height_of_trianlge, int *weidth_of_triangle)
{
    T_area->height=*height_of_trianlge;
    T_area->weidth=*weidth_of_triangle;
    printf("Height and weidth of Triangle is: %d and %d\n",T_area->height,T_area->weidth);
    T_area->area=(*height_of_trianlge * *weidth_of_triangle);
    T_area->print=&Print_area;
}

void Rectangle(area_t* R_area,int *length_of_rect, int *weidth_of_rect)
{
    R_area->length=*length_of_rect;
    R_area->weidth=*weidth_of_rect;
    printf("Length and weidth of a Rectangle are:%d & %d\n",R_area->length,R_area->weidth);
    R_area->area= ((*length_of_rect * *weidth_of_rect)/2);
    R_area->print=&Print_area;
}



