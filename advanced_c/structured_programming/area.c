// defining union
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

typedef union area
{
    float* length;
    float* height;
    float* weidth;
    float* radius;
    float* area;
    float (*fun_ptr)(union area *);
}area_t;

void Square(area_t* S_area, int *length);
void Circle(area_t* C_area, int *radius);
void Triangle(area_t* T_area, int *height, int *weidth);
void Rectangle(area_t* R_area,int *length, int *weidth);

void Print_area(union area* area);
//void Square(area_t* S_area);
//void Circle(area_t* C_area);
//void Triangle(area_t* T_area);
//void Rectangle(area_t* R_area);



int main()
{
    int choice;
    printf("Enter the the geometry you need to find area of:\n");
    printf("press\t1->Square\n\t2->Circle\n\t3->Triangle\n\t4->Rectangle\n");
    scanf("%d",&choice);

    area_t area_to_find;
    switch (choice)
    {
        case 1:
        {
            int length=3;
            //scanf("%d",&length);
            Square(&area_to_find,&length);
            area_to_find.fun_ptr(&area_to_find);
            break;
        }

        case 2:
        {
            int radius;
            scanf("%d",&radius);
            Circle(&area_to_find,&radius);
            area_to_find.fun_ptr(&area_to_find);
            break;
        }

        case 3:
        {
            int height,weidth;
            scanf("%d %d",&height,&weidth);
            Triangle(&area_to_find, &height, &weidth);
            area_to_find.fun_ptr(&area_to_find);
            break;        
        }

        case 4:
        {
            int  length ,weidth;
            scanf("%d %d",&length,&weidth);
            Rectangle(&area_to_find, &length, &weidth);
            area_to_find.fun_ptr(&area_to_find);
            break;        
        }

        default:
            printf("Invalid Input");
            break;
    }

    return EXIT_SUCCESS;
}


void Square(area_t* S_area, int* length)
{
    S_area->length=*length;
    S_area->area= ((*length) * (*length));
    printf("Length = %d\n",S_area->length);
    S_area->fun_ptr=Print_area;
}

void Circle(area_t* C_area, int *radius)
{
    float pi=3.14;
    float* pie = &pi;
    C_area->radius=*radius;
    C_area->area=2;// (*pie * *radius * *radius);
    C_area->fun_ptr=Print_area;
}

void Triangle(area_t* T_area, int *height, int *weidth)
{
    T_area->height=*height;
    T_area->weidth=*weidth;
    T_area->area=(*height * *weidth);
    T_area->fun_ptr=Print_area;
}

void Rectangle(area_t* R_area,int *length, int *weidth)
{
    R_area->length=length;
    R_area->weidth=weidth;
    R_area->area= ((*length * *weidth)/2);
    printf("Length of Rect = %d\n",R_area->length);
    printf("Weidth of Rect = %d\n",R_area->weidth);
    printf("Area = %d\n",R_area->area);
    R_area->fun_ptr=Print_area;
}

void Print_area(union area* area)
{
    printf("Area is: %d\n",area->area);
}
