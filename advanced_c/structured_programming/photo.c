#include<stdlib.h>
#include<stdio.h>

//----------------------------------------------------------------
// STRUCT Photo
//----------------------------------------------------------------
typedef struct Photo
{
    char* Title;
    char* Description;
    void (*print)(struct Photo*);
}Photo;

//----------------------------------------------------------------
// FUNCTION PROTOTYPES
//----------------------------------------------------------------
void new_photo(Photo* photo, char* Title, char* Description);
void print_photo(struct Photo* photo);

void print_photo(struct Photo* photo)
{
    printf("Title:       %s\n", photo->Title);
    printf("Description: %s\n", photo->Description);
}

//----------------------------------------------------------------
// FUNCTION NewPhoto
//----------------------------------------------------------------
void new_photo(Photo* photo, char* Title, char* Description)
{
    // Properties
    photo->Title = Title;
    photo->Description = Description;

    // Methods
    photo->print = print_photo;
}

int main(int argc, char* argv[])
{
    puts("------------------------------------");
    puts("| codedrome.com                    |");
    puts("| Object Oriented Programming in C |");
    puts("------------------------------------\n");

    Photo p1;
    new_photo(&p1, "Thing", "Photograph of a thing");
    p1.print(&p1);

    Photo p2;
    new_photo(&p2, "Another Thing", "Photograph of another thing");
    p2.print(&p2);

    Photo p3;
    new_photo(&p3, "Yet Another Thing", "Photograph of yet another thing");
    p3.print(&p3);
    
    return EXIT_SUCCESS;
}
