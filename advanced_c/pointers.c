#include<stdio.h>
#include<stdlib.h>

int Multiply(int *a, int *b)
{
	int *c= malloc(sizeof(int));
	*c=*a * *b;
	return c;
}

int main()
{
	int a=3, b=5;
	int *c = Multiply(&a,&b);
	printf("Product is %d\n",*c);
	return 0;
}
