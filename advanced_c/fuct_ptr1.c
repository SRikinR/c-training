#include<stdio.h>

int ascArray(int arr[], int size)
{    
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if(arr[j]>arr[j+1])
            {
                int temp;
                temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
}

int desArray(int arr[],int size)
{
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if(arr[j]<arr[j+1])
            {
                int temp;
                temp = arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
}

int printArray(int arr[],int size)
{
    printf("Array = ");
    for(int i=0; i<size; i++)
    {
        printf("%d ",arr[i]);
    }
    printf("\n");
}

int decision(int value,int arr[], int num)
{
    if(value ==0)
    {
        printf("Printing it in ascending order\n");
        ascArray(arr,num);
    }
    else if(value ==1)
    { 
        printf("Printing it in descending order\n");
        desArray(arr,num);
    }
}


int main()
{
    int num;
    int value;
    int choice;
    int i;
    printf("Enter the values of an array\n");
    scanf("%d",&num);
    printf("Size = %d\n",num);
    int arr[num];
    int (*func_ptr_arr[])(int arr[],int) = {ascArray,desArray};   
    for(i=0; i<num; i++)
    {
        scanf("%d",&arr[i]);
//        printf("%d",arr[i]);
    }
//       setVal(&arr,num);
    
    printArray(arr,num);

    printf("Enter your choice:\n0-> Asc\n1->des\n");
    scanf("%d",&choice);
    
    //decision(choice,arr,num);
    func_ptr_arr[choice](arr,num);

    printArray(arr,num);

    return 0;
}
