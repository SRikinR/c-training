#include<stdio.h>
#include<stdlib.h>


int Multiply(int *a, int *b)
{
    int *c= malloc(sizeof(int));
    *c=(*a) * (*b);
    printf("Value of c is: %d\n",c);
    return *c;
}

int ADD(int *a, int*b)
{
    int *d=malloc(sizeof(int ));
    *d =(*a)+(*b);
    printf("Value of d is: %d\n",d);
    return *d;
}

int main()
{
    int a=3;
    int b=5;
    int *c;
    int *d;
    //*c= Multiply(&a,&b);
    int (*p) (int *a, int *b) = Multiply;
    int (*p_arr[])(int *, int *) = {Multiply, Multiply};
    *c = p(&a,&b);
    printf("Product is: %d\n",*c);

    *c=p_arr[0](&a,&b);
    printf("Product is: %d\n",*c);

    *d=p_arr[1](&a,&b);
    printf("Adision is: %d\n",*d);

    return 0;
}