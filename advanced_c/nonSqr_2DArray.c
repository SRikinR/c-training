#include<stdio.h>
#include<stdlib.h>

int main()
{
	float **ptr=(float**)calloc (5,sizeof(float*));
	int i;

	for(i=0; i<5; i++)
	{
		ptr[i]=(float*)calloc(i+1,sizeof(float));
	}

	for(int i=0;i<5;i++)
	{
		for(int j=0; j<i+1; j++)
		{
			printf("%f ",ptr[i][j]);
		}
		printf("\n");
		free(ptr[i]);
	}
	free(ptr);
	
	return 0;
}
