#include<stdio.h>
#include<signal.h>
#include<unistd.h>

//invokes myhandler whenever CTRL-C is pressed
void my_handler()
{
  //Return type of the handler function should be void
  printf("\nInside Interrupt handler function: \n");
}

void my_handler1()
{
    printf("\nInside Quit Handler Funciton\n");
}

void my_handler2()
{
    printf("\nInside Interrupt-2 handler Function\n");
}

int main()
{
  signal(SIGINT,my_handler); // Register signal handler
  signal(SIGQUIT,my_handler1); 

  for(int i=1;;i++)
  {    //Infinite loop
    printf("%d : Inside main function\n",i);
    sleep(1);  // Delay for 1 second
  }
  return 0;
}