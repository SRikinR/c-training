#include<stdio.h>
#include<math.h>


union area
{
    float length;
    float height;
    float weidth;
    float radius;
};
typedef union area area_t;

int main()
{
    int choice;
    printf("Enter the the geometry you need to find area_t of:\n");
    printf("press\t1->Square\n\t2->Circle\n\t3->Triangle\n\t4->Rectangle\n");
    scanf("%d",&choice);

    if (choice == 1)
    {
        area_t Square;
        printf("Enter the Height/Weidth of a Square:\t");
        scanf("%f",&Square.weidth);
        printf("Area = %f\n",(Square.weidth*Square.weidth));
    }

    else if (choice == 2)
    {
        area_t Circle;
        printf("Enter the Radius of a Circle:\t");
        scanf("%f",&Circle.radius);
        printf("Area = %f\n",(3.12*(Circle.radius*Circle.radius)));
    }

    else if (choice == 3)
    {
        area_t Triangle;
        printf("Enter the Height and weidth/Base of a Triangle:\t");
        scanf("%f%f",&Triangle.height, &Triangle.weidth);
        printf("Area = %f\n",(((Triangle.height) * (Triangle.weidth))/2)); // h*b / 2
    }

    else if (choice == 4)
    {
        area_t Rectangle;
        printf("Enter the Length and weidth of a Rectangle:\t");
        scanf("%f%f",&Rectangle.length, &Rectangle.weidth);
        printf("Area = %f\n",((Rectangle.length) * (Rectangle.weidth)));
    }

    else
    {
        printf("Invalid Input\n");
    }

    return 0;
}
