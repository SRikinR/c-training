/** @file hello.h
 *  @brief This code contains, the declaration of function (print hello and sum) 
 *
 *  This contains the prototype and eventually any macros, constants,
 *  or global variables you will need.
 *
 *  @author Shah Rikin R.
 *  @bug No known bugs.
 */

#ifndef _MY_HELLO_H
#define _MY_HELLO_H

/** @brief Prints Hello world to the console.
 *
 *  @param ch the character to print
 *  @return The input character
 *
 */

int printHello(void);

/** @brief Adds two number.
 *
 *  Take 2 Value from the cursor and print the sum to the console
 *  
 *  @param a and 
 *  @param b are two int values whose sum will be stored in
 *  @param c [c=a+b]
 *  @return c, The sum of two values.
 */

int sum(int a, int b);


#endif /* _MY_HELLO_H */
