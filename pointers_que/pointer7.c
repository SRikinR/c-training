//7: Write a function that returns a pointer to the maximum value of an array of double's.  If the array is empty, return NULL.//  double* maximum(double* a, int size);

#include<stdio.h>

double * maximum(double* arr, int size)
{
    if (size>0)
    {    
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if(arr[j]<arr[j+1])
            {
                int temp;
                temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
    printf("Maximum no. in an array is: %0.2f\n",arr[0]);
    }
    else
    {
        printf("NULL");
    }
}

int main()
{
    double size=5;
    double arr[5]={1,2,7.8,7.5};
    double *ptr;
    ptr = &arr[0];
    maximum(ptr,size);    
    return 0;
}
