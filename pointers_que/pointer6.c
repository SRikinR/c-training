//6: Write a function countEven(int*, int) which receives an integer array and its size, and returns the number of even numbers in the array

#include<stdio.h>
#include<stdlib.h>

int countEven(int* array,int size)
{
    static int count;
    for(int i=0; i<size; i++){
        if(array[i]%2==0)
        {
            printf("%d ",array[i]);
            count++;
        }
    }
    printf("\n");    
    return count;
}

int printArr(int *array,int size)
{
    for(int i=0; i<size; i++)
    {
        printf("%d ",array[i]);
    }
    printf("\n"); 
}
int main()
{
    int size;
    printf("Enter the size of and array\n");
    scanf("%d",&size);

    int arr[size];
    int *ptr;
    ptr=&arr[0];
    for(int i=0; i<size; i++)
    {
        scanf("%d",&ptr[i]);
    }
    printf("\n"); 
    
    printArr(ptr,size);

    int count=countEven(ptr,size);
    printf("Even numbers in the array are: %d\n",count);

    return 0;
}
