//8: Write a function myStrLen(char*) which returns the length of the parameter cstring.  Write the function without using the C++ function strlen.

#include<stdio.h>

int myStrLen(char *ptr)
{
    static int count;
//    int size = sizeof(ptr);
    for(int i=0; i>=0; i++)
    {
        if(ptr[i]=='\0'){ break;}
        count++;
    }
    return count;
}

int main()
{
    char cstring[100];
    char *ptr = &cstring[0];
    printf("Enter the string:\n");    
    gets(cstring);
    puts(cstring);
//    printf("Size: %d\n",sizeof(ptr));
    int length = myStrLen(ptr);
    printf("\nLength of the cstring is: %d\n",length);
    
    return 0;
}
