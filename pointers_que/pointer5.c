//5: For the following functions, use the pointer notation ONLY.  Do NOT use the array index [] notation.

//Write a piece of code which prints the characters in a cstring in a reverse order.

#include<stdio.h>

int main()
{
char s[10] = "abcde";
char* cptr;
cptr = &s[0];
for(int i=5; i>=0; i--)
{
printf("%c",*(cptr+i));
}
printf("\n");
return 0;
}
// WRITE YOUR CODE HERE
