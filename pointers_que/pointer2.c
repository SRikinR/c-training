//2: Consider the following statements:
#include<stdio.h>

int main()
{
    int *p;
    int i;
    int k;
    //int *k;
    i = 42;
    k = i;
    //k=&i;
    p = &i;
    //After these statements, which of the following statements will change the value of i to 75?

    k = 75;
    printf("1) i=%d\n",i);
    //(*k) = 75;
    //printf("i=%d\n",i);
    
    //p = 75;
    //printf("2) i=%d\n",i);
    
    //int q=75;
    (*p) = 75;
    printf("3) i=%d\n",i);
    //Two or more of the answers will change i to 75.
}
