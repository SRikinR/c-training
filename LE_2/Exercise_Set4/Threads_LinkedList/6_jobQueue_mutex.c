#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<pthread.h>

struct job
{
    struct job* next;
}

struct job *job_queue;

pthread_mutex_t MUTEX1 = PTHREAD_MUTEX_INITIALIZER;

void *Thread_func(void *arg)
{
  while(1)
  {
    struct job* next_job;
    pthread_mutex_lock(&MUTEX1);

    if(job_queue==NULL)
    {
        next_job=NULL;
    }
    else
    {
        next_job=job_queue;
        job_queue=job_queue->next;
    }

    pthread_mutex_unlock(&MUTEX1);
    
// check wheather the queue is empty, if yes then break
    if(next_job==NULL)
    {
        break;
    }

// if queue not empty then carry out the Process
    Process_job(next_job);
    free(next_job);
  }
  return NULL;
}

int main()
{
    
    return 0;
}
