#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<pthread.h>
#include<semaphore.h>

struct job
{
    struct job* next;
};

struct job* job_queue;

pthread_mutex_t MUTEX1 = PTHREAD_MUTEX_INITIALIZER;
sem_t job_queue_count;

struct job *Process_job(struct job* job_to_do)
{

}

void Initialize_job_queue(void)
{
    // initially job queue is empty
    job_queue=NULL;
    sem_init(&job_queue_count,0,0); // intial value of ssemaphore is zero
}

void *Thread_func(void * arg)
{
    while(1)
    {
        struct job* next_job;
       
        // if the count is +ve value, that indicates queue is not empty. decrement the count by 1
        sem_wait(&job_queue_count);        
        pthread_mutex_lock(&job_queue);
    
        next_job=job_queue;
        job_queue=job_queue->next;
    
        pthread_mutex_unlock(&job_queue);
        
        Process_job(next_job);
        free(next_job);
    }
    return NULL;
}

void Enqueue_job(void)
{
    struct job* new_job=(struct job*)malloc(sizeof(struct job));

    pthread_mutex_lock(&MUTEX1);
    new_job->next=job_queue;
    job_queue=new_job;

    sem_post(&job_queue_count);
    pthread_mutex_unlock(&MUTEX1);

}

int main()
{
    
    return 0;
}
