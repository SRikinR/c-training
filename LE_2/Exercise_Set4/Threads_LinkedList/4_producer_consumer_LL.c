#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<semaphore.h>
#include<unistd.h>
#include<assert.h>

#define MAX 5

struct job
{
    struct job* next;
};

struct job* job_queue;
struct job* job_queue_ptr;

pthread_mutex_t MUTEX1 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t COND1 = PTHREAD_COND_INITIALIZER;
sem_t job_queue_count;
int count;

void Initial(void)
{
    job_queue=NULL;
    sem_init(&job_queue_count,0,0); // intialized semaphore to 0
}

void *Enqueue(void *arg)
{
    for(int i=0; i<10; i++)
    {
        struct job *new_job=(struct job*)malloc(sizeof(struct job));

        pthread_mutex_lock(&MUTEX1);
        //pthread_cond_broadcast(&COND1);
        
        new_job->next=job_queue;
        job_queue=new_job;
        sem_post(&job_queue_count);
        sem_getvalue(&job_queue_count,&count);
        printf("Queue Block %d Created\n",count);
        //sleep(2);
        pthread_mutex_unlock(&MUTEX1);
    }
    pthread_exit(NULL);
}

void *Dequeue(void *arg)
{
    pthread_detach(pthread_self());
    while(1)
    {
        struct job* new_job;

        sem_wait(&job_queue_count);
        assert(pthread_mutex_lock(&MUTEX1)==0);
        //assert(pthread_cond_wait(&COND1,&MUTEX1)==0);

        new_job=job_queue;
        job_queue=job_queue->next;
        sem_getvalue(&job_queue_count,&count);
        printf("Queue Block dequeued: %d\n",count);
        assert(pthread_mutex_unlock(&MUTEX1)==0);
    }
    pthread_exit(NULL);
}

int main()
{
    Initial();
    
    pthread_t producer;
    pthread_t consumer[MAX];

    for(int i=0; i<MAX; i++)
    {
        pthread_create(&consumer[i],NULL,Dequeue,NULL);
    }

    pthread_create(&producer,NULL,Enqueue,NULL);
    pthread_join(producer,NULL);

    while(count>0)
    {
        for(int i=0; i<MAX; i++)
        {
            pthread_cancel(consumer[i]);
        }
    }

    pthread_mutex_destroy(&MUTEX1);
    pthread_cond_destroy(&COND1);
    sem_destroy(&job_queue_count);

    return 0;
}


