/*
    implementing jobs without using mutex, which can create problem if two threads completes the process at the same time
    (Race Condition occurs- Mutual Exculsion error)
    
*/


#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<pthread.h>

struct job
{
    struct job* next;
};

struct job* job_queue;
struct job *Process_job(struct job* jobs_to_do);

void *ThreadFunc(void *arg)
{
    while(job_queue!=NULL)
    {
        struct job* next_job = job_queue;
        job_queue = job_queue->next;
    
        Process_job(next_job);
        free(next_job);
    }
}

int main()
{
    job_queue=(struct job*)malloc(sizeof(struct job));
    struct job *job1=(struct job*)malloc(sizeof(struct job));
    struct job *job2=(struct job*)malloc(sizeof(struct job));
    
    job_queue->next=job1;
    job1->next=job2;
    job2->next=NULL; 

    return 0;
}
