#include<stdio.h>
#include<pthread.h>
#include<assert.h>
#include<unistd.h>

#define MAX_CONSUMER 10

//Total Threads = 1-Producer, 10-Consumer,  1-mutex and 1-cond. thread

pthread_mutex_t gMUTEX1 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t gCOND1 = PTHREAD_COND_INITIALIZER;

void *ProducerThread(void *arg);
void *ConsumerThread(void *arg);

int gWorkCount=0;

int main()
{
    pthread_t producer;
    pthread_t consumer[MAX_CONSUMER];

    //creating Consumer's thread
    for(int i=0; i<MAX_CONSUMER; i++)
    {
        pthread_create(&consumer[i],NULL,ConsumerThread,NULL);
    }

    pthread_create(&producer,NULL,ProducerThread,NULL);
    pthread_join(producer,NULL);

    while(gWorkCount>0)
    {
        for(int i=0; i<MAX_CONSUMER; i++)
        {
            pthread_cancel(consumer[i]);
        }
    }

    pthread_mutex_destroy(&gMUTEX1);
    pthread_cond_destroy(&gCOND1);
        
    return 0;
}


void *ProducerThread(void *arg)
{
    int ret;
    double result=0.0;
    int count=0; 
    for(int i=0; i<30; i++)
    {
        ret=pthread_mutex_lock(&gMUTEX1);
        if(ret==0)
        {
            count++;
            printf("Producer: Creating Work %d\n",gWorkCount);
            gWorkCount++;
            pthread_cond_broadcast(&gCOND1);
            pthread_mutex_unlock(&gMUTEX1);
        }
        else
        {
            assert(0);
        }
        if(count>5)
        {
            sleep(5);
            count=0;
        }
    }

    
    printf("Producer Finished\n");
    pthread_exit(NULL);
}

void *ConsumerThread(void *arg)
{
    int ret;
    pthread_detach(pthread_self());
    
    printf("Consumer %lx started\n",pthread_self());

    while(1)
    {
        assert(pthread_mutex_lock(&gMUTEX1)==0);
        assert(pthread_cond_wait(&gCOND1, &gMUTEX1)==0);

        if(gWorkCount)
        {
            gWorkCount--;
            printf("Consumer %lx, Performed Work %d\n",pthread_self(),gWorkCount);
        }
        assert(pthread_mutex_unlock(&gMUTEX1)==0);
        sleep(1);
    }
    printf("Consumer %lx Finished\n",pthread_self());
    pthread_exit(NULL);
}
