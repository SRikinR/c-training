#include<stdio.h>
#include<pthread.h>
#include<assert.h>

#define MAX 10

pthread_mutex_t gMutex1 = PTHREAD_MUTEX_INITIALIZER;
long gProc_Variable=0L;

void *My_func1(void *arg)
{
   int i,ret;
   
   for(int i=0; i<10000; i++)
   {
        ret = pthread_mutex_lock(&gMutex1);
        assert(ret==0);

        gProc_Variable++;

        ret=pthread_mutex_unlock(&gMutex1);
        assert(ret==0);
   }
   pthread_exit(NULL);
}

int main()
{
    pthread_t mythread[MAX];
    int ret;

    for(int i=0;i<MAX;i++)
    {
        pthread_create(&mythread[i],NULL,My_func1,NULL);
        if(ret!=0)
        {
            printf("Error Creating the thread: %d\n",(int)mythread[i]);
        }
    }

    for(int i=0; i<MAX; i++)
    {
        ret = pthread_join(mythread[i],NULL);
        if(ret!=0)
        {
            printf("Error Joining the thread: %d\n",(int)mythread[i]);
        }
    }

    printf("Protected Variable value is: %ld\n",gProc_Variable);
    
    ret = pthread_mutex_destroy(&gMutex1);
    if(ret!=0)
    {
        printf("Unable to destroy Mutex\n");
    }

    return 0;
}

