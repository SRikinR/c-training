#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<assert.h>
#include<unistd.h>

#define max 5

void *My_func1(void *arg)
{
    int index=*((int *)arg);
    printf("Creating Thread %d\n",index);
    sleep(6-index);
    printf("Thread %d Terminating\n",index);
    pthread_exit(NULL);
}

int main()
{
    pthread_t mythread[max];
    int thread_arg[max];
    int ret;    

    for(int i=0; i<max; i++)
    {
        thread_arg[i]=i;
        ret=pthread_create(&mythread[i],NULL,My_func1,&thread_arg[i]);
        assert(!ret);
        printf("In Main: Creating Thread %d\n",i);
    }
    printf("All Theads are Created\n");


    for(int i=0; i<max; i++)
    {
        ret=pthread_join(mythread[i],NULL);
        assert(!ret);
        printf("In Main: Thread %d Terminated\n",i);
    }
    printf("All Theads are Terminated\n");

    return 0;
}
