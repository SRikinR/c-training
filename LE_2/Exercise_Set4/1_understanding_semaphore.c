#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<semaphore.h>
#include<unistd.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
sem_t semaphore1;
sem_t semaphore2;

FILE *fptr;

void Initialize()
{
	sem_init(&semaphore1,0,0);
	sem_init(&semaphore2,0,0);
}

void *thread1_func(void *arg)
{
    pthread_mutex_lock(&mutex1);
	printf("Check1\n");
	printf("Check2\n");
	sem_wait(&semaphore1);
	printf("Check3\n");
	fptr=fopen("number.txt","x+");
	for(int i=0; i<11; i++)
	{
		printf("Check4\n");
//		fprintf(fptr,"# %d\n",i);
	}
//	fclose(fptr);
    sem_post(&semaphore2);
	printf("Check5\n");
    pthread_mutex_unlock(&mutex1);
	pthread_exit(NULL);
}

void *thread2_func(void *arg)
{

	printf("Check6\n");
//	sem_wait(&semaphore1);
	printf("Check7\n");
	sem_post(&semaphore1);
	sem_wait(&semaphore2);
	printf("Check8\n");
	pthread_exit(NULL);
}

int main()
{
	Initialize();

	pthread_t thread1;
	pthread_t thread2;

	pthread_create(&thread1,NULL,thread1_func,NULL);
	pthread_create(&thread2,NULL,thread2_func,NULL);

	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);

	return 0;
}

