#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<semaphore.h>
#include<unistd.h>

FILE *fptr;
int i,j;
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
sem_t semaphore1;
sem_t semaphore2;

void Initialize()
{
	sem_init(&semaphore1,0,1);
	sem_init(&semaphore2,0,1);
}

void *thread1_func(void *arg)
{
    
    sem_wait(&semaphore1);
    
    for(i=0; i<10; i++)
    {
        printf("check1\n"); 
    }
    sem_post(&semaphore2);
}

void *thread2_func(void *arg)
{

    sem_wait(&semaphore2);
    
    for(j=0; j<10; j++)
    {
        printf("check2\n");
//        fprintf(fptr,"$ %d\n",i);
    }
    sem_post(&semaphore1);

}

int main()
{
    fptr=fopen("number.txt","x+");
	Initialize();

	pthread_t thread1;
	pthread_t thread2;

	pthread_create(&thread1,NULL,thread1_func,NULL);
	pthread_create(&thread2,NULL,thread2_func,NULL);

	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);

	return 0;
}

