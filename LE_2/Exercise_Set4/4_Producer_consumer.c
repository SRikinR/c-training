#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>
#include<semaphore.h>

#define max_thread 3

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

sem_t full;
sem_t empty;

void Initial_value()
{
    sem_init(&full,0,0);
    sem_init(&empty,0,1);
}

void *producer(void *arg)
{
    pthread_mutex_lock(&mutex1);
    sem_wait(&empty);       // empty=0 means full

    printf("data added\n");
    sem_post(&full);
    
    pthread_mutex_unlock(&mutex1);

    pthread_exit(NULL);
}

void *consumer(void *arg)
{
    pthread_mutex_lock(&mutex1);
    sem_wait(&full);
    printf("thread, reading data\n");
    sem_post(&empty);


    pthread_mutex_unlock(&mutex1);
    pthread_exit(NULL);
}

int main()
{
    Initial_value();

    pthread_t thread1;
    pthread_t thread2[max_thread];

    pthread_create(&thread1,NULL,producer,NULL);

    for(int i=0; i<max_thread; i++)
    {
        pthread_create(&thread2[i],NULL,consumer,NULL); 
        pthread_join(thread2[i],NULL); 
    }
     
    pthread_join(thread1,NULL);

    pthread_mutex_destroy(&mutex1);
    sem_destroy(&full);
    sem_destroy(&empty);

    return 0;
}
