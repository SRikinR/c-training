#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<semaphore.h>

#define chair 5

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

sem_t barber;   //full (barber's chair is full)
sem_t customer; //empty (barber's chair is empty)

void Initialize(void)
{
    sem_init(&barber,0,0);          //barber semaphore intialized to 1
    sem_init(&customer,0,1);        // ... to 0
}

void *barber_job(void *arg)
{
    pthread_mutex_lock(&mutex1);

    sem_wait(&customer);        // it checks customer count and decrements, if customer = 0  it blocks the thread until its 1. 
    sem_wait(&barber);

    printf("Getting Hair-cut\n");
    printf("Hair-cut done\n");
    sem_post(&barber);
//    printf("Customer leaves, barber waiting for another customer...\n");
    pthread_mutex_unlock(&mutex1);
    
    pthread_exit(NULL);

}

void *customer_job(void *arg)
{
    pthread_mutex_lock(&mutex1);

    sem_post(&customer);
    printf("New Customer Entered\n");

    sem_post(&barber);
    printf("barber avaliable for haircut\n");
    
    pthread_mutex_unlock(&mutex1);

    pthread_exit(NULL);
}

int main()
{
    Initialize();
    
    pthread_t thread1;
    pthread_t thread2[5];

    pthread_create(&thread1,NULL,barber_job,NULL);

    for(int i=0; i<5; i++)
    {
        pthread_create(&thread2[i],NULL,customer_job,NULL);
    }

    pthread_join(thread1,NULL);
    

    pthread_mutex_destroy(&mutex1);
    sem_destroy(&barber);
    sem_destroy(&customer);

    return 0;
}
