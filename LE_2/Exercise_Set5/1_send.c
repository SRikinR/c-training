#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>

#define MAX_LIMIT 512

struct my_message
{
    long int message_type;
    char new_message[MAX_LIMIT];
};

int main()
{
    int running=1;
    int msgid;
    char buffer[50];
    struct my_message new_data; 

    msgid=msgget((key_t)135791,0666|IPC_CREAT);
    if(msgid==-1)
    {
        printf("message queue not created\n");
        exit(0);
    }    

    while(running)
    {
        printf("Enter data: \t");
        fgets(buffer,50,stdin);
        
        new_data.message_type=1;
        strcpy(new_data.new_message,buffer);
        msgsnd(msgid,(void *)&new_data,MAX_LIMIT,0);

        if(strncmp(buffer,"end",3)==0)
        {
            running =0;
        }
    }

    return 0;
}
