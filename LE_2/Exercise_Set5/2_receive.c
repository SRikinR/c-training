#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>

#define MAX_LIMIT 512

struct my_message
{
    long int message_type;
    char new_message[MAX_LIMIT];
};

int main()
{
    int running=1;
    struct my_message new_data;
    long int msg_rec=0;
    int msgid;

    msgid=(msgget((key_t)135791,0666|IPC_CREAT));
   
    while(running)
    {
        msgrcv(msgid,(void*)&new_data,MAX_LIMIT,msg_rec,0);
        printf("Data Received: %s\n",new_data.new_message);

        if(strncmp(new_data.new_message,"end",3)==0)
        {
            running =0;
        }
    } 

    return 0;
}
