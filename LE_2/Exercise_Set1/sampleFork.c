#include<sys/types.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>

int main()
{
    pid_t ret;
    int status=0, i;
    int role;
    ret=fork();
    role=-1;
    if(ret>0)
    {
        printf("Parent Process us running\n");
        for(int i=0;i<5;i++)
        {
            printf("Parent Process Running:%d\n",i);
            sleep(1);
        }
        ret = wait(&status);
        role=0;
    }
    
    else if(ret == 0)
    {
        printf("Child Process is running\n");
        for(int i=0;i<5;i++)
        {
            printf("child Process Running:%d\n",i);
            sleep(1);
        }
        role=1;
    }
    
    if(role==0)
    {
        printf("Parent Exited\n");
    }
    else if(role==1)
    {
        printf("child Exited\n");
    }
    

    return 0;
}

