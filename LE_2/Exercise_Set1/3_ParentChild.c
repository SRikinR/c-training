#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<errno.h>

int main()
{
    pid_t ret;
    int status,i, role;
    role=-1;
    ret=fork();
    pid_t Parent_Id=getppid();
    pid_t Child_Id=getpid();
    printf("\n%s\n",(ret==0)? "Child Process Started": " ");
    printf("Parent Process ID: %d\n",Parent_Id);
    printf("Child Process ID: %d\n",Child_Id);    
    printf("Process Completed...\n");    

    return 0;
}
