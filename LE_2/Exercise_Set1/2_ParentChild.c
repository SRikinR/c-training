#include<sys/types.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>

int main()
{
    pid_t ret;
    int status,i;
    int role=-1;
    ret=fork();
    if(ret>0)
    {
        printf("Parent Process Started\n");
        ret=wait(&status);
        role=0;

    }
    else if(ret ==0)
    {
        printf("Child Process Started\n");
        for(int i=0;i<10001;i++)
        {
            printf("%d\n",i);
        }
        role=1;
    }
    else
    {
        printf("fork() not working\n");
    }
    
    printf("\n%s: Exiting\n",(role==0? "Parent": "child"));

    return 0;
}
