#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>
#include<stdlib.h>

void Sigstp()
{
    signal(SIGTSTP,Sigstp);
    printf("Child, I have received Signal (ctrl+Z)\n");
}

void Sigint()
{
    signal(SIGINT,Sigint);
    printf("Child, I have received Signal (ctrl+C)\n");
}

void Sigquit()
{
    signal(SIGQUIT,Sigquit);
    printf("Child, I have received Signal (ctrl - '\' )T\n");
    exit(0);
}

int main()
{
    int pid;

    if ((pid = fork()) < 0) {
        perror("fork");
        exit(1);
    }

    else if(pid ==0)
    {
        printf("Child Process Started\n");
        signal(SIGTSTP,Sigstp);
        signal(SIGINT, Sigint);
        signal(SIGQUIT, Sigquit);
        for (;;);
    }

    else 
    {
        printf("Parent Sending Signal, SIGHUP\n");
        kill(pid,Sigstp);
        sleep(2);    
       
        printf("Parent Sending Signal, SIGINT\n");
        kill(pid,Sigint);
        sleep(2);    

        printf("Parent Sending Signal, SIGQUIT\n");
        kill(pid,Sigquit);
        sleep(2);    

    }

    return 0;
}
