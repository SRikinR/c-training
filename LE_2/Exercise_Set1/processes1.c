#include<stdio.h>
#include<unistd.h>

int main()
{
    pid_t mypid;
    pid_t myParentPid;
    uid_t myUserId;
    gid_t myGroupId;

    mypid=getpid();
    myParentPid=getppid();
    myUserId=getuid();
    myGroupId=getgid();

    printf("My Process Id is: %d\n",mypid);
    printf("My Parent Process Id is: %d\n",myParentPid);
    printf("My user Id is: %d\n",myUserId);
    printf("My group Id is: %d\n",myGroupId);    

    return 0;
}
