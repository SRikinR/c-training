#include<stdio.h>
#include<sys/types.h>
#include<sys/syscall.h>
#include<unistd.h>

int main()
{
    unsigned cpu,node;

    syscall(SYS_getcpu, &cpu, &node, NULL);
    printf("This Program is running on the CPU %u, and the Node %u\n",cpu,node);

    return 0;
}

