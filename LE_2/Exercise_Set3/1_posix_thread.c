#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<pthread.h>
#include<unistd.h>

#define num 5

void *Myfunc1(void *arg)
{
    int index=*((int *)arg);
    printf("Thread %d running\n",index);
    sleep(6-index);
    printf("Thread %d Terminating\n",index);
    return NULL;
}

int main()
{
    pthread_t mythread[num];
    int thread_arg[num];
    int ret;

//  Creating threads one by one 
    for(int i=0; i<num; i++)
    {
        thread_arg[i]=i;
        printf("In Main: Creating Thread %d\n",i);
        ret=pthread_create(&mythread[i],NULL,Myfunc1,&thread_arg[i]);
        assert(!ret); 
    }
    printf("All the task are created\n");

// wait for each thread to terminate
    for(int i=0; i<num; i++)
    {
        ret=pthread_join(mythread[i],NULL);
        assert(!ret);
        printf("In Main: Thread %d Terminated\n",i);
    }
    printf("All the task are Terminated\n");

    return 0;
}
