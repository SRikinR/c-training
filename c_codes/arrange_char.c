/** @file arrange_char.c
 *  @brief This code contains, Arranging the char in the sentence based on their numerics
 * 	
 Q)	Given a sentence S of size N where each word is a concatenation of a numeric part followed by 	  a bunch of characters. The task is to arrange the words in increasing order of their numeric 	   part.
 *
 *	Examples:
 *
 *	Input: S = “24-amazing 7-coders 11-are”
 * 	Output: coders are amazing
 *	Explanation: order of numeric values are 7, 11, 24. 
 *	So they are arranged accordingly
 *
 *	Input: S = “19-Love 10-I 2001-cricket”
 *	Output: I Love cricket
 *
 *
 *  @author Shah Rikin R.
 *  @bug No known bugs.
 */

#include<stdio.h>

/**  @param no, array[]  
 *	 @brief structure which can store the int and char value. 
 */
struct letters
{
	int no;
	char array[1000];
}l[1000];


void SortData(struct letters l[], int final_count); 
void PrintData(struct letters l[], int final_count);

int main()
{
	int size; /** @param size  @brief size of the sentence */
	scanf("%d",&size);
	char sentence[size]; /** @param @brief sentence[] char variable name sentence with stores the char input */
	
	for(int i=0; i<=size; i++)
	{
	  scanf("%c",&sentence[i]);
	}
	
	int count=0; /** @param @brief count trigers when an index contains int number */
	int final_count; /** @param  @brief final_count keeps count for No. of numbers in the sentence */
	for(int i=0; i<=size; i++)
	{
		if(sentence[i]>'0' && sentence[i]<'9')
	  {
			l[count].no=(sentence[i]-'0');
			//printf("No: %d\n",l[count].no);
			int k=0;
			for(int j=i+1; j<size;j++)
			{
				if(sentence[j]==' ')
				{
					break;
				}
				//printf("%c",sentence[j]);
				l[count].array[k]=sentence[j];
				k++;
			}
			count++;				
			final_count=count;		
			printf(" ");	    
	  }
	}

	/*printf("final count = %d\n",final_count);
	for(int i=0; i<final_count; i++)
	{
		//printf("\n%d->",l[i].no);
		for(int j=0; j<size; j++)
		{
		//	printf("%c",l[i].array[j]);
		}
	}
	PrintData(l,final_count);
	*/

	SortData(l,final_count);
	PrintData(l,final_count);

	return 0;
}

/**  @param l[], final_count, i, j 
 * 	 @brief Sorts the data stored in different structure variable. 
 *   @brief we are comparing l[j].no of all the avaliable structures. 
 */
void SortData(struct letters l[], int final_count)
{
	struct letters temp;
	for(int i=0; i<final_count; i++)
	{
		for(int j=0; j<(final_count-1-i); j++)
		{
			if (l[j].no > l[j + 1].no)
            {
				temp = l[j];
                l[j] = l[j + 1];
                l[j + 1] = temp;
            } 
		}
	}
}

/**  @param l[], final_count, i  
 *	 @brief Prints the data for all the structures 
 */
void PrintData(struct letters l[], int final_count)
{
	printf("\n");
	for(int i=0; i<final_count; i++)
	{
		printf("%s ",l[i].array);
	}	
	printf("\n");
}
