#include<stdio.h>

int main()
{
    int val1,val2;
    printf("Enter Two Values:\n");
    scanf("%d %d", &val1, &val2);

    printf("Entered Values:\nVal1=%d\nVal2=%d\n",val1,val2);
    val1=val1+val2; // 3+2 =5  
    val2=val1-val2; // 5-2=3
    val1=val1-val2; // 5-3=2
    printf("Swapped Values:\nVal1=%d\nVal2=%d\n",val1,val2);

    return 0;
}
