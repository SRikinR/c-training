/*Given an array arr[] of size N. The task is to count the number of unique subsets. 

Examples:

Input: arr[] = {1, 2, 2}
Output: 6
Explanation: Total possible subsets of this set  = 2³= 8. 
Following are the all 8 subsets formed form arr[].
{}, {1}, {2}, {2}, {1, 2}, {1, 2}, {2, 2}, {1, 2, 2}. 
These are all possible subsets out of which {2} and {1, 2} are repeated.
Therefore there are only 6 unique subsets of given set.

Input: arr[] = {1, 3, 3, 4, 4, 4}
Output: 24
*/

#include<stdio.h>
#include<math.h>

struct Array
{
    int set[100];
};

int PrintArray(int arr[], struct Array stored_arr[], int size)
{
    for(int i=0; i<(2^size);i++)
    {
        printf("{");
        for(int j=0; j<size; j++)
        {
            stored_arr[i].set[j]=arr[j];
            printf("%d ",stored_arr[i].set[j]);
        }
        printf("}");
    }
}

int main()
{
    int size;
    printf("Enter the size:\n");
    scanf("%d",&size);
    
    int arr[size];
    struct Array stored_arr[8];

    for(int i=0; i<8;i++)
    {
        for(int j=0; j<size;j++)
        {
            stored_arr[i].set[j]=0;
        }
    }
    
    int count=0;
    int count1=0;
    for(int i=0; i<8;i++)
    {
        count1++;
        for(int j=0; j<size;j++)
        {
            count++;
            stored_arr[i].set[j]=0;
            printf("%d ",stored_arr[i].set[j]);
        }
    }
    printf("Count: %d\n",count);
    printf("Count1: %d\n",count1);

    for(int i=0; i<size; i++)
    {
        scanf("%d",&arr[i]);
    }

    PrintArray(arr,stored_arr,size);
    
    return 0;
}