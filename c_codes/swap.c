#include<stdio.h>

int main()
{
    int val1,val2,tmp;
    printf("Enter Two Values:\n");
    scanf("%d %d", &val1, &val2);
    
    printf("Entered Values:\nVal1=%d\nVal2=%d\n",val1,val2);
    tmp=val1;
    val1=val2;
    val2=tmp;
    
    printf("Swapped Values:\nVal1=%d\nVal2=%d\n",val1,val2);

    return 0;
}
