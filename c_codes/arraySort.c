/** @file arraySort.c
 *  @brief This code contains, Array Sorting, in Asc and Des order.
 *
 *
 *  @author Shah Rikin R.
 *  @bug No known bugs.
 */

#include<stdio.h>

/** @param i, j , size and *arr[] @brief Sorts the array in ascending order
 */
int ascArray(int arr[], int size)
{    
	for(int i=0; i<size; i++)
	{
		for(int j=0; j<size-1-i; j++)
		{
			if(arr[j]>arr[j+1])
			{
				int temp;
				temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
	}
}

/** @param i, j , size and *arr[] @brief Sorts the array in descending order
 */
int desArray(int arr[],int size)
{
	for(int i=0; i<size; i++)
	{
		for(int j=0; j<size-1-i; j++)
		{
			if(arr[j]<arr[j+1])
			{
				int temp;
				temp = arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
	}
}

/** @param i, size and *arr[] @brief Prints the array
 */
int printArray(int arr[],int size)
{
	printf("Array = ");
	for(int i=0; i<size; i++)
	{
		printf("%d ",arr[i]);
	}
	printf("\n");
}

/** @param value, num and arr[] @brief Takes the decision and calls the function accordingly
 */
int decision(int value,int arr[], int num)
{
	if(value ==0)
	{
		printf("Printing it in ascending order\n");
		ascArray(arr,num);
	}
	else if(value ==1)
	{ 
		printf("Printing it in descending order\n");
		desArray(arr,num);
	}
}


int main()
{
	int num;
	int value;
	int choice;
	int i;
	printf("Enter the values of an array\n");
	scanf("%d",&num);
	printf("Size = %d\n",num);
	int arr[num];
	for(i=0; i<num; i++)
	{
		scanf("%d",&arr[i]);
	}

	printArray(arr,num);

	printf("Enter your choice:\n0-> Asc\n1->des\n");
	scanf("%d",&choice);

	decision(choice,arr,num);

	printArray(arr,num);

	return 0;
}
