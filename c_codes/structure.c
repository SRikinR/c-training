#include<stdio.h>
#include<stdlib.h>

typedef struct books
{
    int page;
    char name[20];
    char author[20];
    int price;
}book_t;

struct Node {
	int data;
	struct Node* next;
};

int function1(book_t book_name, book_t book_name1)
{
    printf("Price of book1 is: %d\n",book_name.price);  
    printf("Price of book2 is: %d\n",book_name1.price); 
}


int main()
{
    book_t book1;
    struct books book2;
    book_t* ptr;
    ptr=&book1;
    
    ptr->page=1020;

    scanf("%d",&book1.price);
    scanf("%d",&book2.price);
    
    function1(book1,book2);    
    
    printf("Total pages in book1 is: %d\n",book1.page);
    printf("Displaying the price of book1 using pointers: %d\n", ptr->price);
   
    printf("size of the structure node is: %ld\n",sizeof(struct Node));
    
    
    return 0;
}
