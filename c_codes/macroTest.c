#include<stdio.h>

void function2();
void function3();

#pragma startup function2
#pragma exit function3

void __attribute__((constructor)) function2();
void __attribute__((destructor)) function3(); 

int function1(int a, int b)
{
  int c;
  c=a+b;
  printf("A + B = %d\n",c);
}

void function3(void)
{
  printf("Function3 Running:\n");
}

void function2(void)
{
  printf("Function2 Running\n");
}


int main()
{
  int a,b,c;
  printf("Enter the value for a and b: \n");
  scanf("%d %d", &a, &b);
  function1(a,b);
  return 0;
}
