#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {

	/* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	char *s=(char *)malloc(1024 * sizeof(char));
	scanf("%s",s);
	int size=strlen(s);
	int arr[10];
	printf("size = %d\n",size);
    
	for(int i=0; i<10; i++)
	{
		arr[i]=0;
	}

	for(int i=0; i<size;i++)
	{
		if(s[i]>='0' && s[i]<='9')
		{
			arr[(s[i]-'0')]++;
		}
	}

	for(int i=0; i<10; i++)
	{
		printf("%d ",arr[i]);
	}

	return 0;
}

