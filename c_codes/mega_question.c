/*Objective:
Mega Exercise in "C" Language
Synopsis: Develop and demonstrate "A simple e-money transaction system"
Requirement:
1) A single vendor. (2) Multiple buyers.
3) Vendor provides e-money to each buyer (against cash).
4) E-money is a transaction token. Token is a unique & numeric-only code, (may be 4 to 20 digits).
5) Each buyer can get one or multiple token from vendor.
6) Buyer can buy the items from vendor by expensing token(s) transaction
(instead of cash transaction). 7) A token can never be reused once the successful transaction completed.
8) If possible, create an "encrypted token" (optional).

Example scenarios:
• Buyer B1 pays 1000/- and asks 3 tokens for values 300, 500, & 200.
Buyer B2 pays 200/- and asks 2 tokens for values 50 and 200. • Vendor issues 5 unique tokens (t1 to t5) to the buyers against a prepaid
cash
• B1 buys item successfully by using t1 = 300.
• B2 buys item successfully by using t5 = 200.
• B2 tries to buy another items using another token (t4 = 50) but rejected due to "no credit".
• B1 tries to use t1 again to buy another items using t1 again but rejected due to "already consumed token".
Implementation requirement:
a) Create a system which issues the token to buyers, manages credits
debits per buyer, duplicate token detection, etc.
b) The system should save the database in a DB FILE.TXT.
c) The system should log each transaction in a LOG_FILE.TXT.
d) A unique token can consist user identity and remittance (amount).
Testing requirement:
Create test cases and provide test results. */
