#include<stdio.h>
#include<stdlib.h>

int AscArray(int *arr, int size)
{    
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if(arr[j]>arr[j+1])
            {
                int temp;
                temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
}

int DesArray(int *arr,int size)
{
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size-1-i; j++)
        {
            if(arr[j]<arr[j+1])
            {
                int temp;
                temp = arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
}

int Allocation(int *arr,int num,int actual_size)
{
    printf("Enter new size\n");
    int new_num;
    scanf("%d",&new_num);
    arr=(int *)realloc(arr,new_num * sizeof(int));
    actual_size = new_num;
    return actual_size;    
}

int PrintArray(int *arr,int actual_size)
{   
    printf("Array= ");
    for(int i=0; i<actual_size; i++)
    {   
        printf("%d ",arr[i]);
    }
    printf("\n");

}

int main()
{
    int num=0;
    int actual_size=0;
    printf("Enter size of array:\n");
    scanf("%d",&num);
    
//    int *arr = (int *)malloc(num * sizeof(int));
    int *arr = (int *)calloc(num, sizeof(int)); 
    printf("Press 1 to reallocate the size of an array\n");
    int confirmation;
    scanf("%d",&confirmation);
    if(confirmation ==1)
    {
        actual_size=Allocation(arr,num,actual_size);
    }
    else
    {
        actual_size=num;
    }

    for(int i=0;i<actual_size;i++)
    {
        scanf("%d",&arr[i]);
    }
    PrintArray(arr,actual_size);
    
    int (*Func_ptr[])(int *arr, int size)={AscArray,DesArray};
    printf("\n0 for Asc, 1 for Des\n");
    int choice;
    scanf("%d",&choice);
    Func_ptr[choice](arr,actual_size);
    PrintArray(arr,actual_size);    
    
    free(arr);    

    return 0;
}
