/** @file Documentation_Emoney.c
 *  @brief This code contains, Develop and demonstrate "A simple e-money transaction system" code.
 * 	
 *
 *
 *  @author Shah Rikin R.
 *  @bug No known bugs.
 */

#include<stdio.h>

int cash;
int no_of_tokens;

struct token_asked
{
	
	int token_amount;
	int no_of_token;
	int token_id;
	int final_amount;
};

struct buyer
{
	struct token_asked token_array[5];
	int balance;
};

int basic_details()
{
	//int cash;
	printf("\nEnter the amount you need to convert\n");
	scanf("%d",&cash);
	printf("how many tokens you want\n");
	//int no_of_tokens;
	scanf("%d", &no_of_tokens);
}

int avaliable_tokens(int tokens[])
{
	printf("\nAvaliable tokens are:\n");
	for(int i=0; i<5;i++)
	{
		printf("%d ",tokens[i]);  
	}
	printf("\nselect the token no. you want\n");
	for(int i=0; i<5; i++)
	{
		printf("T:%d -> %d\n",i,tokens[i]);
	}
};

int buying(struct buyer buyers[]);

int main()
{
	int seller;
	int tokens[5]={100,200,300,500,1000};
	//int cash;
	//int no_of_tokens;
	FILE *fp;
	FILE *fp1;
	fp = fopen("DB_FILE.txt","w+");
	fp1 = fopen("LOG_FILE","w+");

	printf("\n\tWelcome to the store!!\n");
	printf("\tConvert your Cash into token\n");

	printf("Enter no. of buyers\n");
	int no_of_buyers;
	scanf("%d",&no_of_buyers);
	fprintf(fp,"\nTotal Number of Buyers are: %d\n",no_of_buyers);
	fprintf(fp1,"\nTotal Number of Buyers are: %d\n",no_of_buyers);

	struct buyer buyers[no_of_buyers];
	//struct token_asked token_array[no_of_tokens];    // no. of tokens...

	int sum=0;
	int id= 4001;
	for(int i=0; i<no_of_buyers;i++)
	{
		printf("\n\tBuyer %d:\n",i); 
		basic_details();  
		avaliable_tokens(tokens);
		fprintf(fp,"\nCash Avaliable with Buyer[%d] is %d\n",i,cash);
		fprintf(fp,"Token issued by buyer[%d] are:\n",i);
		fprintf(fp1,"\nCash Avaliable with Buyer[%d] is %d\n",i,cash);
		fprintf(fp1,"Token issued by buyer[%d] are:\n",i);   
        int sum=0;     
		int choice; //selects the token, from the avaliable token array
		for(int j=0; j<no_of_tokens;j++)
		{
			id++;
			printf("T:");
			scanf("%d",&choice);
			buyers[i].token_array[j].token_amount=tokens[choice];
			//printf("How Much Quantity?\n");
			int quantity=1;
			//scanf("%d",&quantity);

			buyers[i].token_array[j].no_of_token= quantity;
			buyers[i].token_array[j].token_id=id;
			fprintf(fp,"\nBuyer[%d]: Token amount %d, Token id=%d\n",i,buyers[i].token_array[j].token_amount,buyers[i].token_array[j].token_id);
			fprintf(fp1,"\nBuyer[%d]: Token amount %d, Token id=%d\n",i,buyers[i].token_array[j].token_amount,buyers[i].token_array[j].token_id);

			//----checking balance
			sum=sum+buyers[i].token_array[j].token_amount;
			buyers[i].balance=sum;
			fprintf(fp1,"Current Token Balance of Buyer[%d] is: %d\n",i,buyers[i].balance);
            buyers[i].balance=cash;
		}
	}
	//buying(buyers);
	int want_to_buy=1;
	while(want_to_buy==1)
	{
		printf("who is buying, buyer");
		int buyer_no;
		scanf("%d",&buyer_no);
		buyers[buyer_no];
		printf("Your product cost:\n");
		int product_cost;
		scanf("%d",&product_cost);
		fprintf(fp1,"\nBuyer[%d] is buying an item cost %d\n",buyer_no,product_cost);
        //int diff = buyers[buyer_no].balance;
        //printf("diff=%d\n",diff);
		for(int i=0; i<5; i++)
		{   
			if(product_cost == buyers[buyer_no].token_array[i].token_amount)
			{
				if(buyers[buyer_no].token_array[i].token_id != 0 && product_cost<=buyers[buyer_no].balance)
				{
					printf("Thank you, product is whole yours!");
					fprintf(fp1,"Token used: %d, Token id:%d\n", buyers[buyer_no].token_array[i].token_amount,buyers[buyer_no].token_array[i].token_id);
					buyers[buyer_no].balance=buyers[buyer_no].balance-buyers[buyer_no].token_array[i].token_amount;
					buyers[buyer_no].token_array[i].token_id = 0;
					//buyers[buyer_no].balance = diff;
					fprintf(fp1,"Buyer[%d] current balance is: %d\n",buyer_no, buyers[buyer_no].balance);
				}
				else if (buyers[buyer_no].token_array[i].token_id == 0)
				{
					printf("Token Expired\n");
					fprintf(fp1,"Buyer[%d] used the used-token, Transaction not done\n",buyer_no);
				}
				else if (product_cost>=buyers[buyer_no].balance)
				{
					printf("No Credit\n");
					fprintf(fp1,"Buyer[%d] dosen't have enough cash, Transaction not done\n",buyer_no);
				}
			}
		}
		printf("\nPress 0 to quit\nPress 1 to continue buying\n");
		int choice;
		scanf("%d",&choice);
		if (choice ==0)
		{
			want_to_buy=0;
		}
		else
		{
			want_to_buy=1;
		}
	} 
	fclose(fp);
	fclose(fp1);

	return 0;
}

