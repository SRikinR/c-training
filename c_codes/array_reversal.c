#include <stdio.h>
#include <stdlib.h>

void setVal(int *arr, int size){
    for(int i =0; i<size; i++){
        scanf("%d",&arr[i]);
    }
}

int printArr(int *arr, int size){
    for(int i=(size-1); i>=0; i--){
        printf("%d ",arr[i]);
    }
	printf("\n");
	return 0;
}

int main()
{
    int num, *arr, i;
    scanf("%d", &num);
    arr = (int*)malloc(num * sizeof(int));
    setVal(arr, num);
    printArr(arr, num);

    /* Write the logic to reverse the array. */
    return 0;
}
