#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<assert.h>
#define MAX_CHARACTERS 1005
#define MAX_PARAGRAPHS 5

char* kth_word_in_mth_sentence_of_nth_paragraph(char**** document, int k, int m, int n) {
    return document[n-1][m-1][k-1];
}

char** kth_sentence_in_mth_paragraph(char**** document, int k, int m) {
    return document[m-1][k-1];
}

char*** kth_paragraph(char**** document, int k) {
    return document[k-1];
}

char**** get_document(char* text)
{
    char ****doc_pointing_to_paragraph = malloc(sizeof(char ***)*MAX_PARAGRAPHS);   // doc_pointing_to_paragraph is pointing to paragraphs

    for(int i=0; i<MAX_PARAGRAPHS; i++)
    {
        doc_pointing_to_paragraph[i] = malloc(sizeof(char **));  // assingned 1 "sentence" array for every paragraph
        for(int j=0; j<1; j++)
        {
            doc_pointing_to_paragraph[i][j]=malloc(sizeof(char*)); //assigned 1 "word" array for every sentence
            for(int k=0; k<1; k++)
            {
                doc_pointing_to_paragraph[i][j][k]=malloc(sizeof(char)); // assigned 1 "character" array for every word
            }
        }
    }

    int i_para=0,j_sentence=0,k_word=0,l_character=0;
    for(int n=0; n<strlen(text); n++)
    {
        //store character
        if( text[n] != ' ' && text[n]!= '.' && text[n]!='\n')
        {
            doc_pointing_to_paragraph[i_para][j_sentence][k_word][l_character] = text[n];
            l_character++;
            doc_pointing_to_paragraph[i_para][j_sentence][k_word] = realloc(doc_pointing_to_paragraph[i_para][j_sentence][k_word], sizeof(char)*(l_character+1));
        }

        //word
        else if ( text[n] == ' ')
        {
            doc_pointing_to_paragraph[i_para][j_sentence][k_word][l_character] = '\0';
            l_character=0;
            k_word++;
            doc_pointing_to_paragraph[i_para][j_sentence] = realloc(doc_pointing_to_paragraph[i_para][j_sentence], sizeof(char *)*(k_word+1));
            doc_pointing_to_paragraph[i_para][j_sentence][k_word] = malloc(sizeof(char));
            continue;
        }

        //sentence
        else if (text[n] == '.')
        {
            doc_pointing_to_paragraph[i_para][j_sentence][k_word][l_character] = '\0';
            l_character=k_word=0;
            j_sentence++;
            doc_pointing_to_paragraph[i_para] = realloc(doc_pointing_to_paragraph[i_para], sizeof(char**)*(j_sentence+1));
            doc_pointing_to_paragraph[i_para][j_sentence]=malloc(sizeof(char*));
            doc_pointing_to_paragraph[i_para][j_sentence][k_word] = malloc(sizeof(char));
            continue;
        }

        //paragrah
        else if(text[n] == '\n')
        {
            j_sentence=k_word=l_character=0;
            i_para++;
            continue;
        }
    }
    return doc_pointing_to_paragraph;
}

char* get_input_text() {
    int paragraph_count;
    scanf("%d", &paragraph_count);

    char p[MAX_PARAGRAPHS][MAX_CHARACTERS], doc[MAX_CHARACTERS];
    memset(doc, 0, sizeof(doc));
    getchar();
    for (int i = 0; i < paragraph_count; i++) {
        scanf("%[^\n]%*c", p[i]);
        strcat(doc, p[i]);
        if (i != paragraph_count - 1)
            strcat(doc, "\n");
    }

    char* returnDoc = (char*)malloc((strlen (doc)+1) * (sizeof(char)));
    strcpy(returnDoc, doc);
    return returnDoc;
}

void print_word(char* word) {
    printf("%s", word);
}

void print_sentence(char** sentence) {
    int word_count;
    scanf("%d", &word_count);
    for(int i = 0; i < word_count; i++){
        printf("%s", sentence[i]);
        if( i != word_count - 1)
            printf(" ");
    }
}

void print_paragraph(char*** paragraph) {
    int sentence_count;
    scanf("%d", &sentence_count);
    for (int i = 0; i < sentence_count; i++) {
        print_sentence(*(paragraph + i));
        printf(".");
    }
}

int main()
{
    char* text = get_input_text();
    char**** document = get_document(text);

    int q;
    scanf("%d", &q);

    while (q--) {
        int type;
        scanf("%d", &type);

        if (type == 3){
            int k, m, n;
            scanf("%d %d %d", &k, &m, &n);
            char* word = kth_word_in_mth_sentence_of_nth_paragraph(document, k, m, n);
            print_word(word);
        }

        else if (type == 2){
            int k, m;
            scanf("%d %d", &k, &m);
            char** sentence = kth_sentence_in_mth_paragraph(document, k, m);
            print_sentence(sentence);
        }

        else{
            int k;
            scanf("%d", &k);
            char*** paragraph = kth_paragraph(document, k);
            print_paragraph(paragraph);
        }
        printf("\n");
    }
}
