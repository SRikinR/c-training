#include<stdio.h>

int cash;
int no_of_tokens;

struct token_asked
{
	int token_amount;
	int no_of_token;
	int token_id;
	int final_amount;
};

struct buyer
{
	struct token_asked token_array[5];
	int balance;
};

int basic_details()
{
	//int cash;
	printf("\nEnter the amount you need to convert\n");
	scanf("%d",&cash);
	printf("how many tokens you want\n");
	//int no_of_tokens;
	scanf("%d", &no_of_tokens);
}

int avaliable_tokens(int tokens[])
{
	printf("\nAvaliable tokens are:\n");
	for(int i=0; i<5;i++)
	{
		printf("%d ",tokens[i]);  
	}
	printf("\nselect the token no. you want\n");
	for(int i=0; i<5; i++)
	{
		printf("T:%d -> %d\n",i,tokens[i]);
	}
};

int buying(struct buyer buyers[]);

int main()
{
	int seller;
	int tokens[5]={100,200,300,500,1000};
	//int cash;
	//int no_of_tokens;
    FILE *fp;
    FILE *fp1;
    fp = fopen("DB_FILE.txt","w+");
    fp1 = fopen("LOG_FILE","w+");

	printf("\n\tWelcome to the store!!\n");
	printf("\tConvert your Cash into token\n");

	printf("Enter no. of buyers\n");
	int no_of_buyers;
	scanf("%d",&no_of_buyers);
    fprintf(fp,"\nTotal Number of Buyers are: %d\n",no_of_buyers);
    fprintf(fp1,"\nTotal Number of Buyers are: %d\n",no_of_buyers);

	struct buyer buyers[no_of_buyers];

	//struct token_asked token_array[no_of_tokens];    // no. of tokens...

	int sum=0;
	int id= 4001;
	for(int i=0; i<no_of_buyers;i++)
	{
		printf("\n\tBuyer %d:\n",i); 
		basic_details();  
		avaliable_tokens(tokens);
        fprintf(fp,"\nCash Avaliable with Buyer[%d] is %d\n",i,cash);
        fprintf(fp,"Token issued by buyer[%d] are:\n",i);
        fprintf(fp1,"\nCash Avaliable with Buyer[%d] is %d\n",i,cash);
        fprintf(fp1,"Token issued by buyer[%d] are:\n",i);        
        int sum=0;
		int choice; //selects the token, from the avaliable token array
		for(int j=0; j<no_of_tokens;j++)
		{
			id++;
			printf("T:");
			scanf("%d",&choice);
			buyers[i].token_array[j].token_amount=tokens[choice];
			//printf("How Much Quantity?\n");
			int quantity=1;
			//scanf("%d",&quantity);

			buyers[i].token_array[j].no_of_token= quantity;
			buyers[i].token_array[j].token_id=id;
			fprintf(fp,"Buyer[%d]: Token amount %d, Token id=%d\n",i,buyers[i].token_array[j].token_amount,buyers[i].token_array[j].token_id);
			fprintf(fp1,"Buyer[%d]: Token amount %d, Token id=%d\n",i,buyers[i].token_array[j].token_amount,buyers[i].token_array[j].token_id);
            
            //----checking balance
            buyers[i].token_array[j].final_amount=(buyers[i].token_array[j].token_amount*buyers[i].token_array[j].no_of_token);
            sum=sum+buyers[i].token_array[j].final_amount;
            buyers[i].balance=sum;
            fprintf(fp1,"\nCurrent Balance of Buyer[%d] is: %d\n",i,buyers[i].balance);
		}
	}
	buying(buyers); 
    fclose(fp);
    fclose(fp1);

	return 0;
}


int buying(struct buyer buyers[])
{
	int want_to_buy=1;
	while(want_to_buy==1)
	{
		printf("who is buying, buyer");
		int buyer_no;
		scanf("%d",&buyer_no);
		buyers[buyer_no];
		printf("Your product cost:\n");
		int product_cost;
		scanf("%d",&product_cost);
		for(int i=0; i<5; i++)
		{   
			if(product_cost == buyers[buyer_no].token_array[i].token_amount)
			{
				if(buyers[buyer_no].token_array[i].token_id != 0 && product_cost<=buyers[buyer_no].balance)
				{
					printf("Thank you, product is whole yours!");
					buyers[buyer_no].token_array[i].token_id = 0;
                    buyers[buyer_no].balance=buyers[buyer_no].balance-product_cost;
				}
				else if (buyers[buyer_no].token_array[i].token_id == 0)
				{
					printf("Token Expired\n");
				}
                else if (product_cost>=buyers[buyer_no].balance)
                {
                    printf("No Credit\n");
                }
			}
		}
		printf("\nPress 0 to quit\nPress 1 to continue buying\n");
		int choice;
		scanf("%d",&choice);
		if (choice ==0)
		{
			want_to_buy=0;
		}
		else
		{
			want_to_buy=1;
		}
	}
}