#include<stdio.h>

int cash;
int no_of_tokens;

struct token_asked
{
    int token_amount;
    int no_of_token;
    int token_id;
    int final_amount;
};

struct buyer
{
    struct token_asked token_array[5];
    int count;  // no_of_unique_tokens per buyer
    int balance;
};

int basic_details()
{
    //int cash;
    printf("\nEnter the amount you need to convert\n");
    scanf("%d",&cash);
    printf("how many tokens you want\n");
    //int no_of_tokens;
    scanf("%d", &no_of_tokens);
}

int avaliable_tokens(int tokens[])
{
    printf("\nAvaliable tokens are:\n");
    for(int i=0; i<5;i++)
    {
        printf("%d ",tokens[i]);
        
    }
};

int main()
{
    int seller;
    int tokens[5]={100,200,300,500,1000};
    //int cash;
    //int no_of_tokens;


    printf("\nWelcome to the store!!\n");
    printf("Convert your Cash into token\n");

    printf("Enter no. of buyers\n");
    int no_of_buyers;
    scanf("%d",&no_of_buyers);
    struct buyer buyers[no_of_buyers];

    avaliable_tokens(tokens);

    //struct token_asked token_array[no_of_tokens];    // no. of tokens...

    printf("\n\nEnter the token amount and no. of tokens need\nEX: 200 3 (means you need 3 200-tokens)\n");
    int sum=0;
    for(int i=0; i<no_of_buyers;i++)
    {
        printf("\nBuyer %d:\n",i); 
        basic_details();  
        sum =0;
        for(int j=0; j<no_of_tokens;j++)
        {
            buyers[i].count++;
            scanf("%d %d",&buyers[i].token_array[j].token_amount,&buyers[i].token_array[j].no_of_token);
            buyers[i].token_array[j].final_amount=buyers[i].token_array[j].token_amount*buyers[i].token_array[j].no_of_token; 
            sum = sum + buyers[i].token_array[j].final_amount;
        }
        buyers[i].balance=sum;
    }

    for(int i=0; i<no_of_buyers;i++)
    {
        printf("\nBalance for Buyer %d = %d\n",i,buyers[i].balance);
    }	
    return 0;
}
