#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
	struct node *next;
};

typedef struct node node;

void Initlist(void **, int, int );
void InsertNodeAt(void  *,int, int);
void Displaylist(void*);
void DeleteNode(void **, int );
int search_sll(void*, int );
void sort_sll(void**);
