/**
 * @file api.c
 * @brief singly Linked List Library Code
 * @author rikin
 * @version 1
 * @date 2022-04-28
 */
#include<stdio.h>
#include<stdlib.h>
#include "api.h"

int flag;
int max_size_of_queue=5;
int data;
int node_position;

//static struct Node *head=NULL;
static struct Node *r=NULL;

int Initlist(void **head_ref,int max_size_of_queue, int flag)
{
    struct Node *head =*head_ref;
    printf("Implement you list as queue or stack\nPress 1 -->Stack\nPress 2 --> Queue\n");
    scanf("%d",&flag);

    if(flag==1)
    {
        printf("Enter the Data for your list\n");
        printf("\nFlag Status=%d\nList will act as a Stack\n", flag);
        for(int i=0; i<max_size_of_queue; i++)
        {
            struct Node *new_node=(struct Node*)malloc(sizeof(struct Node));
            int data;
            scanf("%d",&data);
            new_node->data=data;
            new_node->next=head;
            *head_ref=new_node;
        }
    }
    else if(flag ==2)
    {
        printf("Enter the Data for your list\n");
        printf("\nFlag Status=%d\nList will act as a Queue\n", flag);
        for(int i=0; i<max_size_of_queue; i++)
        {
            struct Node *new_node=(struct Node*)malloc(sizeof(struct Node));
            int data;
            scanf("%d",&data);
            new_node->data=data;
            new_node->next=NULL;
            if(*head_ref==NULL)
            {
                *head_ref=new_node;
            }
            else
            {
                *head_ref=new_node=r;
            }
        }
    }

}

int InsertNode(void *head_ref , int data)
{
    struct Node *head= head_ref;
    if(flag==1)
    {
        struct Node *new_node=(struct Node *)malloc(sizeof(struct Node));
        printf("Enter Data:\t");
        scanf("%d",&data);
        new_node->data=data;
        new_node->next=head;
        head=new_node;
    }
    else
    {
        if(head==NULL)
        {
            printf("Linked List not initialized\n");
            exit(EXIT_FAILURE);
        }
        struct Node *new_node=(struct Node *)malloc(sizeof(struct Node));
        new_node->data=data;
        new_node->next=NULL;
        r->next=new_node;
        r=new_node;
    }
}

int Displaylist(void *head)
{
    struct Node *ptr=(struct Node*)head;
    printf("Stack Elements are:\n");
		while (ptr!=NULL)
		{
			printf("%d ",ptr->data);
			ptr=ptr->next;
		}
		printf("\n");
}

int InsertNodeAt(void*, int data, int node_position);
int DeleteNode(void *);
int DeleteNodeFrom (void *, int node_position); 
int deletelist ( void *);
int ReverseList( void *);   
