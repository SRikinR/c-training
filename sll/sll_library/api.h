#include<stdio.h>
#include<stdlib.h>

struct Node 
{
    int data;
    struct Node *next;
};

int Initlist(void**,int max_size_of_queue, int flag);
int InsertNode(void * , int data);
int Displaylist(void *);
int InsertNodeAt(void*, int data, int node_position);
int DeleteNode(void *);
int DeleteNodeFrom (void *, int node_position); 
int deletelist ( void *);
int ReverseList( void *);    
                 
