#include "header.h"

void Initlist(void **head, int n, int flag)
{
	node* newnode = malloc(sizeof(node));
	newnode->data = n;
	newnode->next = NULL;

	//node** ref_head = (node **)head;
	node* ptr = *head;

	if(*head == NULL)
	{
		*head = newnode;
	}
	else
	{
		while(ptr->next != NULL)
		{
			ptr = ptr->next;
		}
		ptr->next = newnode;
	}
}

void InsertNodeAt(void *ref_head,int n,int p)
{
    node *head=ref_head;
    node *new_node = (node *)malloc(sizeof(node));

    if(p==0)
    {
        printf("index position\n");
        new_node->next=head->next;
        head->next=new_node;
        new_node->data=head->data;
        head->data=n;
    }
    else
    {
        int i=0;
        while(i!=p-1 && head->next!=NULL)
        {
            head=head->next;
            i++;
        }
        new_node->data=n; 
        new_node->next=head->next;
        head->next=new_node;
    }
    printf("Node added at %d = %d in the list\n",p,n);
}

void Displaylist(void* ref_head)
{
	node* head = (node*)ref_head;
	while(head != NULL)
	{
		printf("%d\n",head->data);
		head = head->next;
	}
}

void DeleteNode(void **head, int n)
{
	node* ptr = *head;
	node* prev;
	
	if(*head == NULL)
	{
		printf("linked list is empty\n");
	}
	else if(ptr->data == n)
	{
		*head = ptr->next;
	}
	else
	{
		while(ptr != NULL  && ptr->data != n)
		{
			prev = ptr;
			ptr = ptr->next;
		}
		if(ptr == NULL)
		{
			printf("number is not present in linked lsit\n");
			return;
		}
		prev->next = ptr->next;
	}
}

int search_sll(void* ref_head, int n)
{
	node* head =(node*)ref_head;
	if(head == NULL)
	{
		return 0;
	}
	else
	{
		while(head != NULL && head->data != n)
		{
			head = head->next;
		}

		if(head == NULL)
		{
			return 0;
		}
		return 1;
	}
}

void sort_sll(void** head)
{
	node *ptr = *head, *index = NULL;
	int temp;
	if(ptr == NULL)
	{
		printf("linkedlist is empty\n");
		return;
	}

	while(ptr != NULL)
	{
		index = ptr->next;

		while(index != NULL)
		{
			if(ptr->data > index->data)
			{
				temp = ptr->data;
				ptr->data = index->data;
				index->data = temp;
			}
			index = index->next;
		}
		ptr = ptr->next;
	}
	

}

