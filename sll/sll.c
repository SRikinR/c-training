#include "header.h"

int main()
{
	node *head = NULL;
	int n,flag,num,i=0;
	printf("how many nodes are there in the linked list?\n");
	scanf("%d",&n);

	while(i<n)
	{
		printf("enter number: ");
		scanf("%d",&num);
		Initlist((void **)&head,num,flag);
		i++;
	}

	Displaylist((void*)head);

    printf("\nInsert Node At position:\nEnter Data and Index Position\n");
    int data, position;
    scanf("%d %d",&data,&position);
    InsertNodeAt((void *)head,data,position);

    printf("Elements in the list are\n");
	Displaylist((void *)head);

	return 0;
}
