//Develop a multi threaded producer consumer application. (15 points)

//-->Three thread application, wherein each thread invokes at fixed (for 1st thread 10sec, 2nd thread 20sec and 3rd thread 30sec)

//-->When thread will invoke, it should read next chunk of 1KB from the target file. After reading chunk apply unique encryption scheme (e.g. add 0x50 on every byte of 1kb chunk). Each thread having unique encryption scheme. After encryption, dumps encrypted chunk to another file.

//-->At a time simultaneously only two threads can read from a target file. If at a time two threads are ready to read, then both threads should be able to read data from target file.


#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>
#include<semaphore.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
sem_t sem1,sem2;
FILE *fp1;
FILE *fp2;
int file_accessed=0;
char ch[1025];

void Initialize_semaphore()
{
    sem_init(&sem1,0,2);
    sem_init(&sem2,0,1);
}

struct data
{
    int thread_no;
};

void *thread_function(void *arg)
{
    struct data *thread_info=(struct data*)arg;

    //fp1=fopen("file1.txt","r+");
    //fp2=(file_accessed==0)?fopen("file2.txt","w+"):fopen("file2.txt","a+");
    file_accessed++;

    int while_loop=1;
    while(1)
    {
        file_accessed++;
        sem_wait(&sem1);
        printf("Thread %d\n", thread_info->thread_no);
        //fgets(ch,1024,fp1);
        //printf("File copied successfully\n");
        //fputs(ch, fp2);
        if(thread_info->thread_no == 1)
        {
            sleep(1);
            sem_post(&sem1);
        }

        else if(thread_info->thread_no == 2)
        {
            sleep(2);
            sem_post(&sem1);
        }

        else if(thread_info->thread_no == 3)
        {
            sleep(3);
            sem_post(&sem1);
        }
        printf("Thread %d is activated again\n", thread_info->thread_no);
        if(file_accessed==5)
        {
            break;
        }
    }

    //sem_post(&sem1);

    //fclose(fp1);
    //fclose(fp2);
    pthread_exit(NULL);
}

int main()
{
    Initialize_semaphore();
    pthread_t thread1, thread2, thread3;

    struct data thread1_arg;  
    struct data thread2_arg;  
    struct data thread3_arg;  
    
    thread1_arg.thread_no=1;
    thread2_arg.thread_no=2;
    thread3_arg.thread_no=3;
    

    printf("Semaphore, Mutex and Thread arguments are initialized\nThreads Starts in 10 seconds...\n");
    sleep(1); //thread 1 invokes after 10 sec.
    pthread_create(&thread1,NULL,thread_function,&thread1_arg);
    printf("Thread 1 Created\n");

    sleep(1); // thread 2 invokes after 20 sec.
    pthread_create(&thread1,NULL,thread_function,&thread2_arg);
    printf("Thread 2 Created\n");

    sleep(1); // thread 3 invokes after 30 sec.
    pthread_create(&thread1,NULL,thread_function,&thread3_arg);
    printf("Thread 3 Created\n");

    pthread_join(thread1,NULL);
    pthread_join(thread2,NULL);
    pthread_join(thread3,NULL);

    sem_destroy(&sem1);
    sem_destroy(&sem2);
    pthread_mutex_destroy(&mutex1);

    return 0;
}



