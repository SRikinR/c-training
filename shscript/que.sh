!/bin/bash
clear

toy=1
up_counter=0
a=0

while [ $toy -ge 0 ]
do
   echo "$toy Toy Produced"
   sleep 1
   toy=`expr $toy + 1`
   up_counter=`expr $toy - 1`
   echo "UP-Counter Count is: $up_counter"
done    
