!/bin/sh
clear
process=$*
pid=$(ps -e | grep "$*" | awk '{print $1}')
echo "PID of $process is $pid"
kill $pid

