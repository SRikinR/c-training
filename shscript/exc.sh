!/bin/sh
clear

for f in *
do
	if [[ -x "$f" && $f != "exc.sh" ]]
	then
		echo "Executing $f"
		time ./$f
	else
		echo "$f is not Executable"
	fi
done
