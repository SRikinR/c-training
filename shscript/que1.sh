!/bin/bash
clear

max=100
toy=0
counter=0

while [ $toy -le $max ]
do
    sleep 1
       
    

    read -p "To Zoom in/out the Counter Value. Press -1,0,1,2 as per your need" choice
    if [ $choice = 1 ]
    then
        xdotool key Ctrl+plus        
    elif [ $choice = -1 ]
    then
        xdotool key Ctrl+minus
    elif [ $choice = 2 ] 
    then
        xdotool key Ctrl+plus
        xdotool key Ctrl+plus
    elif [ $choice = '+' ]
    then
        counter=`expr $counter + 1`
        toy=`expr $toy + 1`
        echo "$toy Toy Produced" >> counter.txt
        echo "UP-Counter Count is: $counter" >> counter.txt
        echo " " >> counter.txt
    else
        xdotool key Ctrl+0
    fi
    printf "\n"
    echo "$toy Toy Produced"
    echo "Counter Value is: $counter"
    printf "\n"


done    
